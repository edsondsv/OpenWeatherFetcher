local owf = require 'src/owf-funcs'

if arg[1] == '-setid' then -- configurar ID da API, necessário rodar na primeira vez
	owf.setid(arg[2])
elseif arg[1] == '-setlocale' then -- definir local de onde buscar as informações meteorológicas. Sobrescrito toda vez que requisitado
	owf.setlocale(arg[2])
elseif arg[1] == '-k' then -- definir unidade de temperatura como kelvin
	owf.pretty_print(owf.fetch_data())
elseif arg[1] == '-f' then -- definir unidade de temperatura como fahrenheit
	-- ainda não implementado
elseif arg[1] == '-c' then -- definir unidade de temperatura como celsius
	-- ainda não implementado
elseif arg[1] == '-raw' then
	trash, json = owf.fetch_data()
	trash = nil
	print("=====\n"..json.."\n=====")

end
