Contribute with what you feel is necessary. I'll review every case and merge it with the master arbitrarily.   
   
   
Some points that need improvements:   
* Probably proper scopes for variables   
* Pretty print of data   
* Different temperature scales (C and F aren't implemented yet)   
* Time conversion from unix UTC to legible time format (HH:MM:SS)   
* Some nice ascii art for weather.icon   
* Removal of unecessary information like City ID, Internal parameters, weather ID and others   