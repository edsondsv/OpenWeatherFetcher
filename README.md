# OpenWeatherFetcher

OpenWeatherFetcher (owf for short) is a simple CLI tool to display complete weather information to any location in the world. It uses the API from https://openweathermap.org/ to fetch it's information and display it in a legible format to the User.   
   
   
Important information:   
It is necessary to setup an API ID (you must get that from https://openweathermap.org/api) and to setup a valid location ID in order to use this CLI tool.   
   
To setup the API ID:
```
$lua5.1 owf.lua -setid GIVEN_API
```
   
To setup the Location ID:   
   
1. Locate your city using the following command   
```
$lua5.1 owf.lua -setlocale | less 
```
   
2. Enter the Location ID   
```
$lua5.1 owf.lua -setlocale CITYID
```
   
   
Command:
```
$lua5.1 owf.lua -k
```
   
   
Returned information:
```
>> GEOGRAPHICAL INFORMATION
	LATITUDE: -22
	LONGITUDE: -42.5
	COUNTRY: BR
	STATE ID (ISO 3166): 3451189
	STATE NAME: Estado do Rio de Janeiro
	SUNRISE DATE/TIME: Sunday 12th August 2018 09:15:24 AM (UTC DATE/TIME)
	SUNSET DATE/TIME: Sunday 12th August 2018 08:34:33 PM (UTC DATE/TIME)
>> WEATHER INFORMATION
	STATUS: Clouds
	dESCRIPTION: scattered clouds
>> TEMPERATURE INFORMATION
	TEMPERATURE (KELVIN): 283.394
	HUMIDITY: 92
	ATMOSPHERIC PRESSURE: 933.97
	MINIMUM TEMPERATURE: 283.394
	MAXIMUM TEMPERATURE: 283.394
>> WIND INFORMATION
	WIND SPEED: 1.71
	WIND DEGREES (METEOROLOGICAL): 204.002
>> CLOUDINESS INFORMATION
	CLOUDINESS: 44

```
