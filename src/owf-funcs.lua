local http = require 'socket.http'
local cjson = require 'dkjson'

local _ = {}
------------------------------------------------------------------------
	function _.setid(id)
		if id ~= nil then
			local address = 'https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=%s'
			local address_with_id = address:format(id)
			local website_response = http.request(address_with_id)
			local test_request = cjson.decode(website_response)
			if test_request['cod'] == 401 then
				print(test_request["message"])
			else
				print("WARNING: If there is already an API ID defined for this user, the existent one will be overwrited!")
				io.write("Do you wish to continue? (y/N) ")
				local user_response = io.read()
				if user_response:lower() == "y" then
					local conf_file = io.open(os.getenv("HOME").."/.owf_config/appid.dat", "w")
					if conf_file == nil then
						os.execute("mkdir ~/.owf_config")
						conf_file = io.open(os.getenv("HOME").."/.owf_config/appid.dat", "w")
					end
					conf_file:write(id)
					conf_file:close()
				end
			end
		else
			print("Error setting up the openweathermap.org API ID.")
			print("Did you typed your API ID after -defineid?")
		end
	end
------------------------------------------------------------------------
	function _.print_complex_table(tab, do_plain)
		for i,j in pairs(tab) do
			if type(j) == "table" then
				if not do_plain then print("------------------") end
				_.print_complex_table(j, do_plain)
			else
				print(i..":"..j)
			end
		end
	end
------------------------------------------------------------------------
	function _.search_id(tab, city_id)
		for i, j in pairs(tab) do
			if j['id'] == city_id then
				return true
			end
		end
		return false
	end
------------------------------------------------------------------------
	function _.capitalize(str)
		return (str:gsub("^%l", string.upper))
	end
------------------------------------------------------------------------
	function _.to_celsius(x)
		return x - 273.15
	end
------------------------------------------------------------------------
	function _.to_fahrenheit(x)
		return (x - 273.15) * 1.8000 + 32.00
	end
------------------------------------------------------------------------
	function _.setlocale(city_id)
		local locations = io.open("share/cities.json", "r")
		local loc_table = cjson.decode(locations:read("*all"))
		if city_id ~= nil then
			if _.search_id(loc_table, tonumber(city_id)) then
				local conf_file = io.open(os.getenv("HOME").."/.owf_config/geoid.dat", "w")
				if conf_file == nil then
					os.execute("mkdir ~/.owf_config")
					conf_file = io.open(os.getenv("HOME").."/.owf_config/geoid.dat", "w")
				end
				conf_file:write(city_id)
				conf_file:close()
			else
				print('Provided city ID is invalid, please "run owf --setlocale | less" to find the proper city ID for the desired location')
			end
		else
			_.print_complex_table(loc_table, false)
		end
	end
------------------------------------------------------------------------
	function _.fetch_data()
		local appid_file = io.open(os.getenv("HOME").."/.owf_config/appid.dat", "r")
		local location_file = io.open(os.getenv("HOME").."/.owf_config/geoid.dat", "r")
		local API = appid_file:read("*all")
		local GEOID = location_file:read("*all")
		local address = string.format('https://api.openweathermap.org/data/2.5/weather?id=%s&appid=%s', GEOID, API)
		local website_response = http.request(address)
		if website_response == nil then print("Failed to fetch data, service could be offline")
		else
			local data_table = cjson.decode(website_response)
			if data_table["cod"] ~= 200 then print("GEOID or API ID invalid, please check your conf on ~/.owf_config/")
			else
				return data_table, website_response
			end
		end
	end
------------------------------------------------------------------------
	function _.get_readeable_time(unix_time)
		if unix_time ~= nil then
			local API_address = string.format('http://www.convert-unix-time.com/api?timestamp=%s', unix_time)
			local returned_json = http.request(API_address)
			if returned_json ~= nil then
				local converted_table = cjson.decode(returned_json)
				return string.format('%s (UTC DATE/TIME)', converted_table.utcDate)
			else
				print("API request failed. Are you sure you are online?")
			end
		else
			print("Received Unix Time is empty. This may be caused by a problem in your request to the server or misconfiguration of Open Weather Fetcher")
		end
	end
------------------------------------------------------------------------
	function _.pretty_print(info_table, temp_mode)
		recursive_table = function(raw_data)
        		for i, v in pairs(raw_data) do
                		if type(v) == "table" then
                        		if type(i) ~= "number" then
						dado = tostring(i)
	                        		print(">> "..dado:upper())
	                        		recursive_table(v)
					end
                		else
                        		print(i,v)
                		end
        		end
		end
		recursive_table(info_table)
	end
------------------------------------------------------------------------
return _
